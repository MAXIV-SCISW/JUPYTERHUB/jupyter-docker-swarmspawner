# JUPYTERHUB SWARM SPAWNER

## OVERVIEW

This is a jupyterhub extension that allows JupyterHub to spawn Jupyter
notebooks into a docker cluster.

This code is a modified version of
[SwarmSpawner](https://github.com/rasmunk/SwarmSpawner).


## USING CODE

This code should be installed into the jupyterhub container, like so:
```bash
cd /path/to/jupyter-docker-hub/
vim Dockerfile
RUN pip install \
    git+https://gitlab.maxiv.lu.se/scisw/jupyterhub/jupyter-docker-swarmspawner.git@master
```

The jupyterhub config file used by the jupyterhub docker service needs to also
modified:
```bash
cd /path/to/jupyter-docker-service/
vim maxiv/jupyterhub_config.py
# Set the type of spawner
c.JupyterHub.spawner_class = 'jhub.SwarmSpawner'

# First pulls can be really slow, so let's give it a big timeout
c.SwarmSpawner.start_timeout = 60 * 15

# The name of the service and network
c.SwarmSpawner.jupyterhub_service_name = 'jupyter-service_jupyterhub'
c.SwarmSpawner.networks = ["jupyter-service_default"]

# The jupyter notebook (work) directory
# The keyword {user} will be replaced by the logged-in user's name.
c.SwarmSpawner.notebook_dir = '/home/{user}/jupyter_notebooks/'

# Mount a local directories - the data direcotry and the user's home directory
#     source == on host machine
#     target == in spawned container
# The keyword {user} will be replaced by the logged-in user's name.
mounts = [
    {'type': 'bind',
     'source': '/nfs/offline0/staff/',
     'target': '/data/staff/',
     'read_only': True},
    {'type': 'bind',
     'source': '/nfs/offline0/visitors/',
     'target': '/data/visitors/',
     'read_only': True},
    {'type': 'bind',
     'source': '/mxn/home/{user}/',
     'target': '/home/{user}/',
     'read_only': False},
]

# 'args' is the command to run inside the service
c.SwarmSpawner.container_spec = {
    'args': ['/usr/local/bin/start-singleuser.sh',
             '--NotebookApp.ip=0.0.0.0',
             '--NotebookApp.port=8888'],
    'env': {'JUPYTER_ENABLE_LAB': '1'},
    'mounts': mounts
}

# Available docker images the user can spawn
c.SwarmSpawner.dockerimages = [
    {'image': 'maxiv/hdf5-notebook:latest',
     'name': 'MAXIV HDF5 Notebook'},
    {'image': 'maxiv/base-notebook:latest',
     'name': 'MAXIV Base Notebook'},
]
c.SwarmSpawner.use_user_options = True
```
