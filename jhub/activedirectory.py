"""
Some simple active directory searches.

Using this method for finding all the group id numbers for a user because our
CAS authentication service does not return this information - apparently this
is not possible (or maybe just a bit beyond the general use case for CAS?).

This can also be run from a terminal:

    Setup:
        cd /var/www/jupyter-docker-swarmspawner/
        rm -rf .env/
        virtualenv -p python3 .env
        source .env/bin/activate
        pip install ldap3==2.5.2

    Examples:
        python jhub/activedirectory.py jasbru
        python jhub/activedirectory.py "*service" jasbru zdemat
"""

#####################
# IMPORT LIBRARIES ##
#####################

from ldap3 import Server, Connection, NONE
#        ALL_ATTRIBUTES, ALL_OPERATIONAL_ATTRIBUTES
import logging
import sys
import json
import argparse


class ActiveDirectory(object):

    def __init__(self, debug):
        self.log = logging.getLogger('MyLogger')
        log_level = logging.ERROR
        if debug:
            log_level = logging.DEBUG

        logging.basicConfig(format='%(levelname)s:%(message)s',
                            level=log_level)

    def hello():
        return 'hello'

    def get_ad_config(self, config_path, debug):

        self.log.debug("*** STARTING get_ad_config ***")

        # Dumb way to get the config options?
        config_par = {}
        ad_config_names = 'c.LDAPAuthenticator.'
        with open(config_path) as myfile:
            for line in myfile:
                if line.startswith(ad_config_names):
                    name, var = line.partition("=")[::2]
                    name = name.strip()
                    name = name.replace(ad_config_names, '')
                    var = var.strip()
                    var = str(var.replace("'", ""))
                    var = var.rstrip('/ \\')
                    config_par[name] = var

        if debug:
            for key, value in config_par.items():
                print('  [' + key + ']  =  [' + value + ']')

        self.log.debug("***   END    get_ad_config ***")

        return config_par

    def search_for_attributes(self, username, config_path, attributes_to_keep,
                              debug):

        # Get the AD configuration from jupyterhub_config.py
        config_par = self.get_ad_config(config_path, debug)

        # Connect to the AD server
        server = Server(config_par['server_address'], get_info=NONE)
        conn = Connection(server,
                          user=config_par["lookup_dn_search_user"],
                          password=config_par["lookup_dn_search_password"],
                          lazy=True,
                          read_only=True,
                          check_names=False,
                          )

        self.log.debug("*** executing conn.bind()  ***")
        conn.bind()

        # Search for the given user or group, get the listed attributes
        search_filter = "(" + config_par['user_attribute'] + "=" + \
            str(username) + ")"
        search_base = config_par['user_search_base']
        self.log.debug("search_filter: '{}'".format(search_filter))
        self.log.debug("search_base:   '{}'".format(search_base))

        conn.search(search_base=search_base,
                    search_filter=search_filter,
                    attributes=attributes_to_keep)
        # attributes=[ALL_ATTRIBUTES, ALL_OPERATIONAL_ATTRIBUTES])

        self.log.debug("")
        self.log.debug("RETRIEVED ATTRIBUTES:")
        return conn

    def place_results_in_dictionary_list(self, conn, attributes_to_keep,
                                         debug):

        attribute_dictionary_list = []

        # Loop over the attributes, save most of them
        num_result = 0
        for result in conn.response:
            attribute_dictionary = {}
            self.log.debug("")
            self.log.debug("  result:  '{}'".format(num_result))
            num_result += 1
            if 'attributes' in result:
                for key, value in result['attributes'].items():
                    self.log.debug("  key:  '{}'".format(key))
                    if key == 'jpegPhoto':
                        self.log.debug("  value: <photo junk>")
                    else:
                        self.log.debug("  value:  '{}'".format(value))

                    for attribute in attributes_to_keep:
                        if key == attribute:

                            if key != 'memberOf' and isinstance(value, list):
                                if len(value) > 0:
                                    value = value[0]
                            attribute_dictionary[key] = value
                            self.log.debug("  --> saving")
                attribute_dictionary_list.append(attribute_dictionary)

        # If any of the looked for attributes were not present, set
        # some default value
        for attribute in attributes_to_keep:
            for attribute_dictionary in attribute_dictionary_list:
                if attribute not in attribute_dictionary:
                    attribute_dictionary[attribute] = False

        self.log.debug("")
        self.log.debug("  total_num_result:  '{}'".format(
            len(attribute_dictionary_list)))
        self.log.debug("")
        self.log.debug("SAVED ATTRIBUTES:")
        for attribute_dictionary in attribute_dictionary_list:
            self.log.debug("")
            for key, value in attribute_dictionary.items():
                self.log.debug("  key:    '{}'".format(key))
                self.log.debug("  value:  '{}'".format(value))

        return attribute_dictionary_list

    def parse_group_names_from_ad(self, attribute_dictionary):
        '''
        This function turns output from the AD into a simple group name.
        For example, this:
            [
                "CN=KITS,CN=Users,DC=maxlab,DC=lu,DC=se",
                "CN=Staff,CN=Users,DC=maxlab,DC=lu,DC=se"
            ]
        is reaturned as:
            [
                "KITS",
                "Staff"
            ]
        '''

        self.log.debug("*** STARTING parse_group_names_from_ad ***")

        parsed_list_of_groups = []

        # Parse each group name out of the big mess of groups returned from the
        # AD:
        big_mess_of_groups = []
        if "memberOf" in attribute_dictionary:
            big_mess_of_groups = attribute_dictionary['memberOf']

        self.log.debug("big_mess_of_groups: '{}'".format(
            json.dumps(big_mess_of_groups, indent=2, sort_keys=True)))

        for blob in big_mess_of_groups:
            if 'CN=' in blob and ',' in blob:
                group_name = blob.split('CN=', 1)[1].split(',')[0]

                # Do not keep groups with spaces in their names => windows
                # groups
                if ' ' not in group_name:
                    parsed_list_of_groups.append(group_name)

        self.log.debug("parsed_list_of_groups: '{}'".format(
            json.dumps(parsed_list_of_groups, indent=2, sort_keys=True)))

        self.log.debug("*** ENDING parse_group_names_from_ad ***")

        return parsed_list_of_groups

    def get_default_group(self, attribute_dictionary, debug):

        self.log.debug("*** STARTING get_default_group ***")

        # Look for the uid and gid numbers in the results dictionary
        # Sometimes they can be returned within single element lists
        uid_number = gid_number = -1
        default_group = ''
        if "uidNumber" in attribute_dictionary:
            uid_number = attribute_dictionary['uidNumber']
            if isinstance(uid_number, list):
                if len(uid_number) > 0:
                    uid_number = uid_number[0]
                else:
                    uid_number = -1

        if "gidNumber" in attribute_dictionary:
            gid_number = attribute_dictionary['gidNumber']
            if isinstance(gid_number, list):
                if len(gid_number) > 0:
                    gid_number = gid_number[0]
                else:
                    gid_number = -1

        # They may need to be converted into integers
        uid_number = int(uid_number)
        gid_number = int(gid_number)

        self.log.debug("  uid_number: '{}'".format(uid_number))
        self.log.debug("  gid_number: '{}'".format(gid_number))

        # It seems that all staff members have as their default group MAX-Lab,
        # but this does not show up in the list of 'memberOf' from the AD,
        # although it does show up as a subgroup of Staff.
        # Similarly for visitors:
        # → Should add these to a config file somewhere...
        if gid_number == 1300:
            default_group = 'MAX-Lab'
        if gid_number == 1332:
            default_group = 'Visitors'

        self.log.debug("  default_group: '{}'".format(default_group))

        self.log.debug("***  END   get_default_group ***")

        return uid_number, gid_number, default_group

    def get_user_ad_info(self, username, config_path, debug):

        self.log.debug("*** STARTING get_user_ad_info ***")

        # Initialize variables
        list_of_groups = []
        string_list_of_groups = ""
        string_list_of_groups_num = ""

        # A list of attributes to search for in the AD
        attributes_to_keep = ["uidNumber",
                              "gidNumber",
                              "memberOf",
                              "unixHomeDirectory",
                              "givenName",
                              "sn",
                              "title",
                              "telephoneNumber",
                              "department",
                              "company",
                              "sAMAccountName",
                              "mail"]

        # Perform the search, which returns the connection so that it can be
        # reused
        conn = self.search_for_attributes(username, config_path,
                                          attributes_to_keep, debug)

        # Add the results to a dictionary
        attribute_dictionary_list = self.place_results_in_dictionary_list(
                conn, attributes_to_keep, debug)
        attribute_dictionary = attribute_dictionary_list[0]

        # return

        # Get the uid, gid, and determine the default group
        uid_number, gid_number, default_group = self.get_default_group(
                attribute_dictionary, debug)
        if default_group != "":
            list_of_groups.append(default_group)

        # Parse the group names, add to a list
        primary_list_of_groups = self.parse_group_names_from_ad(
            attribute_dictionary)
        list_of_groups.extend(primary_list_of_groups)

        self.log.debug("")
        self.log.debug("Done with first AD lookup")
        self.log.debug("list_of_groups: '{}'".format(
            json.dumps(list_of_groups, indent=2, sort_keys=True)))

        # return

        # Now get a list of sub-groups, which are groups that groups are
        # members of
        subgroup_list = self.get_subgroup_names(
            list_of_groups, config_path, conn)
        list_of_groups.extend(subgroup_list)

        self.log.debug("")
        self.log.debug("Done with second AD lookup")
        self.log.debug("list_of_groups: '{}'".format(
            json.dumps(list_of_groups, indent=2, sort_keys=True)))

        # return

        # And then one more level ought to be enough
        sub_subgroup_list = self.get_subgroup_names(
            subgroup_list, config_path, conn)
        list_of_groups.extend(sub_subgroup_list)

        self.log.debug("")
        self.log.debug("Done with third AD lookup")
        self.log.debug("list_of_groups: '{}'".format(
            json.dumps(list_of_groups, indent=2, sort_keys=True)))

        # Now remove repeats
        list_of_groups = list(dict.fromkeys(list_of_groups))

        self.log.debug("")
        self.log.debug("Repeated group names removed")
        self.log.debug("list_of_groups: '{}'".format(
            json.dumps(list_of_groups, indent=2, sort_keys=True)))

        # And now get the gid number for each group name
        # If there is not a gid number for a group, then that group is removed
        # from the list
        group_name_gid_dict_list = self.get_gid_numbers(
            list_of_groups, config_path, conn)

        # Put the group names and gid numbers into strings which will
        # eventually be sent to the bash start.sh script
        for group_gid_pair in group_name_gid_dict_list:
            for key, value in group_gid_pair.items():
                string_list_of_groups += str(key) + " "
                string_list_of_groups_num += str(value) + " "

        self.log.debug("***    END   get_user_ad_info ***")

        return uid_number, gid_number, string_list_of_groups, \
            string_list_of_groups_num, attribute_dictionary

    def get_subgroup_names(self, group_names, config_path, conn=False):
        # For each group name, get the corresponding group number, only keep
        # group names for which there is a number.  Also, get rid of groups
        # that have spaces in their names - they're just for windows and of
        # no use to this application

        self.log.debug("*** STARTING get_subgroup_names ***")

        object_class = "group"
        attributes_to_keep = ['sAMAccountName', 'memberOf']

        subgroup_list = self.list_ad_attributes(
            object_class, group_names, attributes_to_keep, config_path,
            conn, False)

        self.log.debug("***    END   get_subgroup_names ***")

        return subgroup_list

    def get_gid_numbers(self, group_names, config_path, conn=False):
        # For each group name, get the corresponding group number, only keep
        # group names for which there is a number.  Also, get rid of groups
        # that have spaces in their names - they're just for windows and of
        # no use to this application

        self.log.debug("*** STARTING get_gid_numbers ***")

        object_class = "group"
        attributes_to_keep = ['sAMAccountName', 'gidNumber']

        subgroup_list = self.list_ad_attributes(
            object_class, group_names, attributes_to_keep, config_path,
            conn, False)

        self.log.debug("***    END   get_gid_numbers ***")

        return subgroup_list

    def get_uid_numbers(self, user_names, config_path, conn=False):
        # For each user name, get the corresponding user number, only keep
        # user names for which there is a number.  Also, get rid of users
        # that have spaces in their names - they're just for windows and of
        # no use to this application

        self.log.debug("*** STARTING get_uid_numbers ***")

        object_class = "user"
        attributes_to_keep = ['sAMAccountName', 'uidNumber']

        subuser_list = self.list_ad_attributes(
            object_class, user_names, attributes_to_keep, config_path,
            conn, False)

        self.log.debug("***    END   get_uid_numbers ***")

        return subuser_list

    def list_all_ad_groups(self, group_names, config_path, conn=False):

        self.log.debug("*** STARTING list_all_ad_groups ***")

        group_name_gid_dict_list = self.get_gid_numbers(
            group_names, config_path)

        list_of_all_groups = list_of_all_group_numbers = ''
        for group_gid_pair in group_name_gid_dict_list:
            for key, value in group_gid_pair.items():
                list_of_all_groups += str(key) + " "
                list_of_all_group_numbers += str(value) + " "

        self.log.debug("***    END   list_all_ad_groups ***")

        return list_of_all_groups, list_of_all_group_numbers

    def list_all_ad_users(self, user_names, config_path):

        self.log.debug("*** STARTING list_all_ad_users ***")

        user_name_uid_list = self.get_uid_numbers(
            user_names, config_path)

        list_of_all_users = list_of_all_user_numbers = ''
        for user_uid_pair in user_name_uid_list:
            for key, value in user_uid_pair.items():
                list_of_all_users += str(key) + " "
                list_of_all_user_numbers += str(value) + " "

        self.log.debug("***    END   list_all_ad_users ***")

        return list_of_all_users, list_of_all_user_numbers

    def list_ad_attributes(self, object_class, object_names,
                           attributes_to_keep, config_path, conn=False,
                           debug=False):

        self.log.debug("*** STARTING list_ad_attributes ***")
        self.log.debug("    object_class: '{}'".format(object_class))
        self.log.debug("    object_names: '{}'".format(object_names))
        self.log.debug("    attributes_to_keep:   '{}'".format(
            attributes_to_keep))

        subgroup_list = []

        # Get the AD configuration from jupyterhub_config.py
        config_par = self.get_ad_config(config_path, debug)

        # Connect to the AD server
        if not conn:
            self.log.debug("    NO CONN")
            server = Server(config_par['server_address'], get_info=NONE)
            conn = Connection(server,
                              user=config_par["lookup_dn_search_user"],
                              password=config_par["lookup_dn_search_password"],
                              lazy=True,
                              read_only=True,
                              check_names=False,
                              )
            self.log.debug("*** executing conn.bind()  ***")
            conn.bind()
        else:
            self.log.debug("    YES CONN")

        # If this is an array of possible username search words, combine them
        object_name_search = ""
        if isinstance(object_names, list):
            if len(object_names) > 0:
                if object_names[0] != '':
                    object_name_search = "|"
                    for object_name in object_names:
                        object_name_search = object_name_search + \
                            "(samaccountname=" + str(object_name) + ")"
        else:
            object_name_search = "samaccountname=" + str(object_names)

        # Search for groups or users
        if object_name_search != "":
            search_filter = "(&(" + str(object_name_search) + \
                ")(objectClass=" + str(object_class) + "))"
            search_base = config_par['user_search_base']
            self.log.debug("search_filter: '{}'".format(search_filter))

            conn.search(search_base=search_base,
                        search_filter=search_filter,
                        attributes=attributes_to_keep)
            # attributes=[ALL_ATTRIBUTES, ALL_OPERATIONAL_ATTRIBUTES])

            attribute_dictionary_list = self.place_results_in_dictionary_list(
                    conn, attributes_to_keep, debug)

            for attribute_dictionary in attribute_dictionary_list:
                group_name = attribute_dictionary['sAMAccountName']
                if ' ' not in attribute_dictionary['sAMAccountName']:
                    self.log.debug("name: '{}'".format(
                        attribute_dictionary['sAMAccountName']))

                    if 'memberOf' in attribute_dictionary:
                        initial_list_of_groups = \
                            self.parse_group_names_from_ad(
                                attribute_dictionary)
                        subgroup_list.extend(initial_list_of_groups)

                    if 'uidNumber' in attribute_dictionary:
                        uidNumber = attribute_dictionary['uidNumber']
                        if uidNumber != []:
                            subgroup_list.append({group_name: uidNumber})

                    if 'gidNumber' in attribute_dictionary:
                        gidNumber = attribute_dictionary['gidNumber']
                        if gidNumber != []:
                            subgroup_list.append({group_name: gidNumber})

            # Remove repeats in a simple list of group names - maybe not needed
            # at this point?
            if len(subgroup_list) > 0:
                if isinstance(subgroup_list[0], str):
                    subgroup_list = list(dict.fromkeys(subgroup_list))

        self.log.debug("subgroup_list: '{}'".format(
            json.dumps(subgroup_list, indent=2, sort_keys=True)))

        self.log.debug("*** ENDING list_ad_attributes ***")

        return subgroup_list


########
# MAIN #
########

def main(argv):
    '''
    The main function - usage and help, argument parsing
    '''

    # Setup options
    parser = argparse.ArgumentParser(
        description='Look up user or group informaiton in an Active Driectory')
    parser.add_argument("input_string", nargs='+',
                        help='The user or group names')
    parser.add_argument("-c", '--config_path', required=False,
                        default=('/var/www/jupyter-docker-service/'
                                 'maxiv/jupyterhub_config.py'),
                        help='Path to the jupyterhub service config file')
    parser.add_argument('-u', '--list_users', action='store_true',
                        help=('Look for user names containing given words, '
                              'return names and uid numbers'))
    parser.add_argument('-g', '--list_groups', action='store_true',
                        help=('Look for group names containing given words, '
                              'return names and gid numbers'))
    parser.add_argument('-d', '--debug', action='store_true',
                        help='debug output')

    # Print a little extra in addition to the standard help message
    if len(argv) == 0 or '-h' in argv or '--help' in argv:
        try:
            args = parser.parse_args(['-h'])
        except SystemExit:
            print('')
            print('Examples of usage:')
            print('  python jhub/activedirectory.py jasbru')
            print('Examples of usage:')
            print('  python jhub/activedirectory.py jasbru zdemat')
            print('Examples of usage:')
            print('  python jhub/activedirectory.py species-service')
            print('Examples of usage:')
            print('  python jhub/activedirectory.py "*service*" "*user*" -u')
            print('Examples of usage:')
            print('  python jhub/activedirectory.py "*max*" balder species -g')
            print('')
            sys.exit()
    else:
        args = parser.parse_args(argv)

    if args.debug:
        print(args)

    ad = ActiveDirectory(args.debug)

    # Get a list of user names and numbers from a list of possible user names
    # or name fragments
    if args.list_users:
        list_of_all_users, list_of_all_user_numbers = \
            ad.list_all_ad_users(args.input_string, args.config_path)

        print()
        print('  USER LIST')
        print('  input:                     ' + str(args.input_string))
        print('  list_of_all_users:         ' + list_of_all_users)
        print('  list_of_all_user_numbers:  ' + list_of_all_user_numbers)
        print()

    # Get a list of group names and numbers from a list of possible group names
    # or name fragments containing wildcards
    elif args.list_groups:

        list_of_all_groups, list_of_all_group_numbers = \
            ad.list_all_ad_groups(args.input_string, args.config_path)

        print()
        print('  GROUP LIST')
        print('  input:                     ' + str(args.input_string))
        print('  list_of_all_groups:        ' + list_of_all_groups)
        print('  list_of_all_group_numbers: ' + list_of_all_group_numbers)
        print()

    # Get user or group information
    # The group names listed will be all nested groups, up to three levels
    else:
        for name in args.input_string:
            uid_number, gid_number, list_of_groups, list_of_group_numbers, \
                attribute_dictionary = \
                ad.get_user_ad_info(name, args.config_path,
                                    args.debug)

            # If the AD info associated with the input name has a uid number,
            # then assume this is a user name, if not assume it is a group name
            if uid_number > 0:
                print()
                print('  USER INFORMATION')
                print('  Name:                  ' +
                      str(attribute_dictionary['givenName']) + ' ' +
                      str(attribute_dictionary['sn']))
                print('  Title:                 ' +
                      str(attribute_dictionary['title']))
                print('  Department:            ' +
                      str(attribute_dictionary['department']))
                print('  Affliation:            ' +
                      str(attribute_dictionary['company']))
                print()
                print('  email:                 ' +
                      str(attribute_dictionary['mail']))
                print('  telephoneNumber:       ' +
                      str(attribute_dictionary['telephoneNumber']))
                print()
                print('  user name:             ' +
                      str(attribute_dictionary['sAMAccountName']))
                print('  uid number:            ' + str(uid_number))
                print('  gid number:            ' + str(gid_number))
                print('  list of groups:        ' + list_of_groups)
                print('  list of group numbers: ' + list_of_group_numbers)
                print('  unixHomeDirectory:     ' +
                      str(attribute_dictionary['unixHomeDirectory']))
                print()
            else:
                print()
                print('  GROUP INFORMATION')
                print('  group name:            ' +
                      str(attribute_dictionary['sAMAccountName']))
                print('  gid number:            ' + str(gid_number))
                print('  sub groups:            ' + list_of_groups)
                print('  sub group numbers:     ' + list_of_group_numbers)
                print()


#######################
# RUN THE APPLICATION #
#######################

if __name__ == '__main__':
    main(sys.argv[1:])
